# Rust AWS Lambda Function

This project outlines the setup for a Rust-based AWS Lambda function with integrated logging and AWS X-Ray tracing, and instructions for connecting the logs and traces to AWS CloudWatch.

## Requirements

- Rust and Cargo installed on your machine
- AWS CLI configured with the necessary permissions
- cargo-lambda installed (`cargo install cargo-lambda`)

## Setup and Deployment

Follow these steps to create, deploy, and verify your Rust Lambda function.

### Step 1: Create a New Lambda Project

Create a new Lambda project using the cargo-lambda tool:

```sh
cargo lambda new <project-name>
```
### step 2: Navigate to Your Project Directory

Change into the project directory:

```
cd <project-name>
```

### step3: Implement Your Function Logic

Modify the main.rs file with the logic for your Lambda function. Implement logging using Rust's log and env_logger libraries and manual tracing for AWS X-Ray.

### Step 4: Test Locally
Test your Lambda function locally with:
```
cargo lambda watch
```

### Step 5: Invoke Your Lambda Function
Invoke your Lambda function with a test payload:
```
cargo lambda invoke --data-ascii '{"key": "value"}'
```
Replace the JSON payload with the input your Lambda function expects.

### Step 6: Configure IAM Role for AWS X-Ray
Ensure that the IAM role used by your Lambda function has the necessary permissions to interact with AWS X-Ray:

- Navigate to the AWS IAM Console.
- Create a new user role
- Attach the `AWSLambda_FullAccess`, `AWSXrayFullAccess`, and `IAMFullAccess` policy to this role.
- Copy the `Access Key`, `Secret Access key` and save in your `.env` file.
- Remember to put `.env` in your `.gitignore` file.
- Use `export` to export all your `.env` variables
  ```
  export AWS_ACCESS_KEY_ID= your access key
  export AWS_SECRET_ACCESS_KEY= your secret key
  export AWS_REGION= your region
  ```
- Build your project and release with
  ```
  cargo lambda build --release
  ```
- Deploy your project with:
  ```
  cargo lambda deploy
  ```

### Step 7: Enable Active Tracing in Lambda

- Go to the AWS Lambda console.
- Select your function and go to the Configuration tab.
- Under Monitoring and operations tools, choose Edit.
- Check the Active tracing box to enable AWS X-Ray tracing.

### Step 9: Verify CloudWatch Logs and X-Ray Traces
After invoking your function:

- Check the CloudWatch Logs to confirm that your logging is working as expected.
Go to the AWS X-Ray console to view the traces generated by your invocations.

### Step 10: Pushing the Project to Gitlab Repo

- Create a blank repo on `Gitlab` without a `README.md`
- Follow the intructions on `Pushing an existing folder`
- Add `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` to your gitlab secrets
- Run jobs.

## Results Preview
![Alt text](img/image-1.png)
![Alt text](img/image-2.png)

## Reference
- https://docs.aws.amazon.com/xray/latest/devguide/xray-services-lambda.html
- https://docs.aws.amazon.com/lambda/latest/dg/monitoring-cloudwatchlogs.html
