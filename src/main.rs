use env_logger;
use lambda_runtime::{handler_fn, Context, Error};
use log::{self, LevelFilter};
use rand::Rng;
use serde_json::{json, Value};

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    log::info!("Received event: {:?}", event);

    let mut rng = rand::thread_rng();
    let random_number: i32 = rng.gen_range(0..1000);
    log::info!("Generated random number: {}", random_number);

    // Placeholder for X-Ray trace log
    log::info!("X-Ray trace: Simulated trace data here");

    Ok(json!({ "random_number": random_number }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    env_logger::builder().filter_level(LevelFilter::Info).init();
    log::info!("Starting the lambda function");
    lambda_runtime::run(handler_fn(function_handler)).await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use lambda_runtime::Context;
    use serde_json::Value;

    #[tokio::test]
    async fn test_function_handler() {
        let event = json!({});
        let context = Context::default();

        let result = function_handler(event, context).await;
        assert!(result.is_ok());

        let value = result.unwrap();
        assert!(value.get("random_number").is_some());
    }
}
